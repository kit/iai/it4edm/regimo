# install required packages with: pip install "oep-client>=0.17"
# import required packages

import logging
from pathlib import Path
from getpass import getpass
from os import environ

from oep_client import OepClient
from oemeta.target_meta import OEMeta
from excel_sheet_adapter import ExcelSheetAdapter

from settings import (MODEL_PATH, LOG_FORMAT, PATH_TO_SOURCE,
                      URL_TO_SOURCE, TOPIC, TARGET_TABLE_NAME,
                      RESOLVED_SCHEMA_FILE_NAME)


def main():
    """
    An excel file containing both data and metadata is uploaded to oep.
    Metadata are attached to the data's table.
    A review process will be triggered, as a result the data are published to
    the databus.open-energy-platform.org.
    """

    # set default value for the oep token
    environ.setdefault('OEP_API_TOKEN',
                       "f00fa56fa4554da3714832a9452525248f9c4988")

    # set values from settings
    path_to_source = PATH_TO_SOURCE
    url_to_source = URL_TO_SOURCE
    model_path = MODEL_PATH
    schema_path = RESOLVED_SCHEMA_FILE_NAME
    table_name = TARGET_TABLE_NAME
    topic = TOPIC
    token = environ.get("OEP_API_TOKEN") or getpass("Enter your OEP API token: ")

    # INFO or DEBUG. Logs are to find under log/
    logging.basicConfig(level=logging.DEBUG, filename='log/' + table_name + '.log',
                        format=LOG_FORMAT, filemode='w')
    logger = logging.getLogger(__name__)
    logger.info('Starting LLEC upload')

    # for read/write, we need to add authorization header  # auth_headers = {"Authorization": "Token %s" % token}
    table_api_url = f"https://openenergyplatform.org/api/v0/schema/{topic}/tables/{table_name}/"
    logger.info("Table API URL: %s" % table_api_url)

    # instantiate table source
    table_source = ExcelSheetAdapter(table_path=Path(path_to_source), table_url=url_to_source,
                                     data_sheet_name='dataset_sample_2rows_comp',
                                     metadata_sheet_name='dataset_sample_2rows_meta')
    # - target metadata
    target_meta = OEMeta(model_path=model_path,
                         schema_path=schema_path)
    # - client to oep
    cli = OepClient(token=token,
                    default_schema=topic)

    # get header from table source
    table_schema_definition = table_source.get_header()

    # load data from table source
    table_data = table_source.get_data()

    # merge metadata from table source into target meta
    ##
    # load metadata header from table source in <sheet[1]|metadata_sheet_name>
    table_metadata_header = table_source.get_metadata_header()
    # example of a field in table_metadata_header
    # {
    #   "index": 0,
    #   "name": "t_amb",
    #   "Description": "Ambient temperature",
    #   "type": "Number",
    #   "isAbout.name": "temperature",
    #   "isAbout.path": "http://openenergy-platform.org/ontology/oeo/OEO_00010453",
    #   "valueReference.value": "temperature",
    #   "valueReference.name": "temperature",
    #   "valueReference.path": "http://openenergy-platform.org/ontology/oeo/OEO_00010453",
    #   "unit": "[°C]"
    # },
    for field_item in table_metadata_header:
        target_meta.merge_field_from_meta_source(field_item)

    general_metadata = table_source.get_metadata_section("general")
    #  example of
    # {'first_or_single': {'name': 'Living Lab Measurements', 'topics': 'Measurement', 'title': 'Living Lab Meas',
    #                      'path': 'https://github.com/koubaa-hmc/LLEC_Data/raw/refs/heads/main/dataset_2rows.xlsx',
    #                      'description': 'The table is a collection of measurements done in a Living Lab',
    #                      'languages': '"en-GB"', 'subject.name': 'energy use',
    #                      'subject.path': 'http://openenergy-platform.org/ontology/oeo/OEO_00010210',
    #                      'keywords': 'http://openenergy-platform.org/ontology/oeo/OEO_00000150',
    #                      'publicationDate': '2025-01-28T00:00:00.000', 'embargoPeriod.start': None,
    #                      'embargoPeriod.end': None, 'embargoPeriod.isActive': False},
    #  'second': {'name': None, 'topics': 'Energy', 'title': None, 'path': None, 'description': None, 'languages': None,
    #             'subject.name': None, 'subject.path': None,
    #             'keywords': 'http://openenergy-platform.org/ontology/oeo/OEO_00000384', 'publicationDate': None,
    #             'embargoPeriod.start': None, 'embargoPeriod.end': None, 'embargoPeriod.isActive': None},
    #  'third': {'name': None, 'topics': 'Temperature', 'title': None, 'path': None, 'description': None,
    #            'languages': None, 'subject.name': None, 'subject.path': None, 'keywords': None, 'pubDate': None,
    #            'embargoPeriod.start': None, 'embargoPeriod.end': None, 'embargoPeriod.isActive': None},
    #  'fourth': {'name': None, 'topics': None, 'title': None, 'path': None, 'description': None, 'languages': None,
    #             'subject.name': None, 'subject.path': None, 'keywords': None, 'publicationDate': None,
    #             'embargoPeriod.start': None, 'embargoPeriod.end': None, 'embargoPeriod.isActive': None}}
    target_meta.merge_general_from_meta_source(general_metadata)

    licenses_metadata = table_source.get_metadata_section("licenses")
    target_meta.merge_licenses_from_meta_source(licenses_metadata)

    provenance_metadata = table_source.get_metadata_section("provenance")
    target_meta.merge_provenance_from_meta_source(provenance_metadata)

    spatial_metadata = table_source.get_metadata_section("spatial")
    target_meta.merge_spatial_from_meta_source(spatial_metadata)

    temporal_metadata = table_source.get_metadata_section("temporal")
    target_meta.merge_temporal_from_meta_source(temporal_metadata)

    target_meta.get_metadata()

    # create table on oep and upload data
    cli.create_table(table_name, table_schema_definition)
    cli.insert_into_table(table_name, table_data)

    # get metadata (from example file)
    # metadata = req.get(
    #    "https://raw.githubusercontent.com/OpenEnergyPlatform/academy/production/docs/data/tutorial_example_table.metadata.json").json()

    # metadata = cli.set_metadata(table, metadata)
    # print(json.dumps(metadata, indent=4))


if __name__ == '__main__':
    main()
