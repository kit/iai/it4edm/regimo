from pathlib import Path
from io import BytesIO

from backend_regimo.components.shared.bend_exceptions import SourceNotFound

import json
import requests as req
import pandas as pd


def _map_type_to_postgres_type(type_literal: str) -> str:
    type_literal = type_literal.lower()
    type_mapping = {
        "int64": "integer",
        "float64": "float",
        "bool": "boolean",
        "datetime64[ns]": "datetime",
    }

    return type_mapping.get(type_literal, "TEXT")


class ExcelSheetAdapter:
    def __init__(self, table_path: Path, table_url: str = "",
                 data_sheet_name: str = "",
                 metadata_sheet_name="") -> None:
        """
        Instantiate an ExcelSheetAdapter object.
        :param table_path: optional, authoritative path of the excel file
        :param table_url: optional, url of the excel file. Not considered if a path is provided
        :param data_sheet_name: optional, name of the data sheet.
        :param metadata_sheet_name: optional, name of the metadata sheet.
        """
        if table_path is not None:
            self.link = table_path
        elif table_url is not None:
            self.link = BytesIO(req.get(table_url).content)
        try:
            self.excel_file = pd.ExcelFile(self.link)
        except FileNotFoundError as error:
            raise SourceNotFound(error)

        self.sheet_names = sheet_names = self.excel_file.sheet_names
        if data_sheet_name is not None:
            if data_sheet_name not in sheet_names:
                raise SourceNotFound()
            else:
                self.data_sheet_name = data_sheet_name
        else:
            self.data_sheet_name = sheet_names[0]
        self.df = pd.read_excel(self.link, engine='openpyxl', sheet_name=data_sheet_name)

        if metadata_sheet_name is not None:
            if metadata_sheet_name not in sheet_names:
                raise SourceNotFound
            else:
                self.metadata_sheet_name = metadata_sheet_name
        else:
            self.metadata_sheet_name = sheet_names[1]
        self.dfm = pd.read_excel(self.link, engine='openpyxl', sheet_name=metadata_sheet_name)

        print(f"This is df-serialization: \n{self.df}")
        print(f"This is dfm-serialization: \n{self.dfm}")

    def get_header(self):
        """
        Read column names from excel file.
        Compose the header in a compatible way to oep.
        :return: table_schema_output as a dictionary composed of an array of columns
        """
        table_schema_source = json.loads(self.df.dtypes.to_json(orient='index', default_handler=str))
        column_dict = table_schema_source.items()
        # column_names = list(table_schema_source.keys())
        table_schema_output = {"columns": [{"name": "id", "data_type": "bigserial", "primary_key": True}]}
        for name, type_literal in column_dict:
            postgres_type = _map_type_to_postgres_type(type_literal)
            json_col = f'{{"name": "{name}", "data_type": "{postgres_type}"}}'
            table_schema_output["columns"].append(json.loads(json_col))
        return dict(table_schema_output)

    def get_data(self):
        """
        Export data from dataframe to a json object.
        :return:
        """
        json_data = self.df.to_json(orient='records', indent=4, date_format='iso')
        return json.loads(json_data)

    def get_metadata_header(self):
        """
        Export metadata header from dataframe to a json object.
        :return:
        """
        json_metadata = self.dfm.to_json(orient='records', indent=4, date_format='iso')
        return json.loads(json_metadata)

    def get_metadata_section(self, section_name: str):
        """
        Export metadata from dataframe to a json.
        :return:
        """
        if section_name in self.sheet_names:
            mdf_section = pd.read_excel(self.link, engine='openpyxl', sheet_name=section_name, index_col=0)
        else:
            raise SourceNotFound("there is no sheet with the name: " + section_name)

        # result_dict = pd.Series(mdf_section.first_or_single.values, index=mdf_section.Property).to_dict()

        json_metadata = mdf_section.to_json(indent=4, date_format='iso')
        return json.loads(json_metadata)
