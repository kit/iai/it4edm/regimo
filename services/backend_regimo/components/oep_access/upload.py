# install required packages with: pip install "oep-client>=0.17"
# import required packages
import json
from random import randint
from getpass import getpass
from os import environ

import requests as req
from oep_client import OepClient

topic = "sandbox"
table = f"tutorial_example_table_{randint(0, 100000)}"
token = environ.get("OEP_API_TOKEN") or getpass("Enter your OEP API token: f00fa56fa4554da3714832a9452525248f9c4988")

# for read/write, we need to add authorization header
auth_headers = {"Authorization": "Token %s" % token}
table_api_url = f"https://openenergyplatform.org/api/v0/schema/{topic}/tables/{table}/"

print(table_api_url)

cli = OepClient(token=token, default_schema=topic)

table_schema = {
    "columns": [
        # NOTE: first column should be numerical column named `id` .
        # Use `bigserial` if you want the database to create the re
        {"name": "id", "data_type": "bigserial", "primary_key": True},
        {"name": "name", "data_type": "varchar(18)", "is_nullable": False},
        {"name": "is_active", "data_type": "boolean"},
        {"name": "capacity_mw", "data_type": "float"},
        {"name": "installation_datetime_utc", "data_type": "datetime"},
        {"name": "location", "data_type": "geometry"},
    ]
}

cli.create_table(table, table_schema)

# get example data
data = req.get(
    "https://raw.githubusercontent.com/OpenEnergyPlatform/academy/production/docs/data/tutorial_example_table.data.json"
).json()

# show results in notebook
print(json.dumps(data, indent=4))

cli.insert_into_table(table, data)

# get metadata (from example file)
metadata = req.get(
    "https://raw.githubusercontent.com/OpenEnergyPlatform/academy/production/docs/data/tutorial_example_table.metadata.json"
).json()

metadata = cli.set_metadata(table, metadata)
print(json.dumps(metadata, indent=4))

