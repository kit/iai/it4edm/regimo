# SPDX-FileCopyrightText: 2024 Ludwig Hülk <@Ludee> © Reiner Lemoine Institut
# SPDX-FileCopyrightText: 2024 Jonas Huber <jh-RLI> © Reiner Lemoine Institut
#
# SPDX-License-Identifier: MIT

from pathlib import Path
from random import randint


LOG_FORMAT = "[%(asctime)s %(module)16s %(levelname)7s] %(message)s"

BASE_PATH = Path("oemeta")
VERSION = "latest"
VERSION_PATH = BASE_PATH / VERSION
SCHEMA_BUILD_PATH = VERSION_PATH / "build_source"
MAIN_SCHEMA_PATH = SCHEMA_BUILD_PATH / "schema_structure.json"
SCHEMA_REFS = SCHEMA_BUILD_PATH / "schemas"
MODEL_PATH = SCHEMA_REFS
RESOLVED_SCHEMA_FILE_NAME = VERSION_PATH / "schema.json"
EXPECTED_SCHEMA_PATH = VERSION_PATH / "schema.json"

EXAMPLE_PATH = VERSION_PATH / "example.json"

PATH_TO_SOURCE = ('/Users/ot2661/Documents/01_dev/aed_pub/regimo/regimo/services/backend_regimo/data' +
                  '/LLEC_Data/dataset_sample_2rows.xlsx')
URL_TO_SOURCE = "https://github.com/koubaa-hmc/LLEC_Data/raw/refs/heads/main/dataset_sample_2rows.xlsx"

TOPIC = "sandbox"
TARGET_TABLE_NAME = f"living_lab_table_{randint(0, 100000)}"
