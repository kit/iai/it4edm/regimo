
import json
from pydantic.networks import AnyUrl
from pydantic import ValidationError

from backend_regimo.components.oep_access.oemeta.latest.build_source.schemas import *


class OEMeta:
    def __init__(self, model_path, schema_path):
        super().__init__()
        self.model_path = model_path
        self.schema_path = schema_path
        self.metadata = {}
        self.linkedData = LinkedDataModel()
        self.general = GeneralModel(name='general_name')
        self.context = ContextModel()
        self.spatial = SpatialModel()
        self.temporal = TemporalModel()
        self.sources = SourceModel()
        self.licenses = LicenseModel()
        self.provenance = ProvenanceModel()
        self.resource = FieldsModel()
        self.resource.schema_ = Schema(primaryKey=[])
        self.resource.schema_.fields = []

    def merge_field_from_meta_source(self, meta_source):
        """
        append a new field to the target meta.
        :param meta_source:
        """
        is_about_url = meta_source["isAbout.path"]
        value_reference_url = meta_source["valueReference.path"]

        # The first iteration is marked by the length of the fields array
        if len(self.resource.schema_.fields) == 0:
            self.resource.type = 'table'
            self.resource.format = 'xlsx'
            self.resource.encoding = 'utf-8'

        current_is_about = IsAboutItem(name=meta_source["isAbout.name"])
        current_is_about.field_id = is_about_url  # AnyUrl(url=is_about_url)

        current_value_reference = ValueReferenceItem(name=meta_source["valueReference.name"])
        current_value_reference.value = meta_source["valueReference.value"]
        current_value_reference.field_id = value_reference_url  # AnyUrl(url=value_reference_url)

        current_field = FieldModel(name=meta_source["name"], type=meta_source["type"], nullable=True)
        current_field.isAbout = [current_is_about]
        current_field.valueReference = current_value_reference
        current_field.unit = meta_source["unit"]
        self.resource.schema_.fields.append(current_field)

    def merge_general_from_meta_source(self, general_metadata):
        name = general_metadata["first_or_single"]["name"]
        description = general_metadata["first_or_single"]["description"]
        publication_date = general_metadata["first_or_single"]["publicationDate"]

        self.general.name = name
        self.general.description = description
        self.general.publicationDate = publication_date

    def merge_licenses_from_meta_source(self, licenses_metadata):
        pass

    def merge_provenance_from_meta_source(self, provenance_metadata):
        pass

    def merge_spatial_from_meta_source(self, spatial_metadata):
        pass

    def merge_temporal_from_meta_source(self, temporal_metadata):
        pass

    def get_metadata(self, metadata_target_path="table_as_current_meta.json"):
        self.metadata["name"] = self.general.name
        self.metadata["title"] = self.general.title
        self.metadata["description"] = self.general.description
        self.metadata["publication_date"] = self.general.publicationDate
        self.metadata["linkedData"] = self.linkedData.model_dump()
        self.metadata["context"] = self.context.model_dump_json()
        self.metadata["spatial"] = self.spatial.model_dump()
        self.metadata["temporal"] = self.temporal.model_dump()
        self.metadata["sources"] = self.sources.model_dump()
        self.metadata["licenses"] = self.licenses.model_dump()
        self.metadata["provenance"] = self.provenance.model_dump()
        self.metadata["resource"] = self.resource.model_dump()

        with open(metadata_target_path, "w") as f:
            f.write(json.dumps(self.metadata, indent=2))
