#!/usr/bin/env fish
for json_file in (ls ../schemas/*.json)
    set filename (basename $json_file)
    echo "Base Filename: " $filename
    set file (string replace -r '.json' '' $filename)
    datamodel-codegen --input $json_file --output $file.py --class-name $file
end