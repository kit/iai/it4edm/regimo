# generated by datamodel-codegen:
#   filename:  review.json
#   timestamp: 2025-02-04T16:03:38+00:00

from __future__ import annotations

from typing import Optional

from pydantic import AnyUrl, BaseModel, Field


class Review1(BaseModel):
    path: Optional[AnyUrl] = Field(
        None,
        description='A link or path to the documented open peer review.',
        examples=[
            'https://openenergyplatform.org/dataedit/view/model_draft/oep_table_example/open_peer_review/'
        ],
        title='Path',
    )
    badge: Optional[str] = Field(
        None,
        description='A badge of either Iron, Bronze, Silver, Gold or Platinum is used to label the quality of the metadata.',
        examples=['Platinum'],
        title='Badge',
    )


class Review(BaseModel):
    review: Optional[Review1] = Field(
        None,
        description='The metadata on the OEP can go through an open peer review process. See the Academy course [Open Peer Review](https://openenergyplatform.github.io/academy/courses/09_peer_review/) for further information.',
        title='Review',
    )
