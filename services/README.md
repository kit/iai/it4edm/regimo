# regimo
## Use Cases
### Use Case #1 (Main use case)
A user plugs in the EDR at a certain location, connects it with its laptop and starts the recording of a campaign. At the same time the user starts the publishing tool REGIMO (from this git repository). All new created files are tracked by the tool and automatically uploaded to the cloud and then published to the databus using its cloud URL.

### Use Case #2 (Secondary use case)
The existing database of the EDR is analyzed and all existing valid data sets (correct filesystem structure and metadata json files exist) get published to the cloud and registered in the databus.

### use
poetry export --without-hashes --format=requirements.txt > requirements.txt

to export an actual requirements.txt